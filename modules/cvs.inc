<?php

function metrics_cvs_metrics_functions() {
  return array(
    'metrics_cvs_maintainer_count',
  );
}

function metrics_cvs_maintainer_count($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('CVS: Maintainer count'),
        'description' => t('If there are more than the specified number of maintainers, returns 1.'),
      );

    case 'compute':
      $count = db_result(db_query('SELECT count(u.uid) FROM {cvs_project_maintainers} cpm INNER JOIN {users} u ON cpm.uid = u.uid WHERE cpm.nid = %d AND u.status = 1', $node->nid));
      if ($count > $options['count']) {
        return array(
          'value' => 1, 
          'description' => format_plural($count, 'There is 1 maintainer.', 'There are @count maintainers.'),
        );
      }
      else {
        return array(
          'value' => 0, 
          'description' => format_plural($count, 'There is only 1 maintainer.', 'There are only @count maintainers.'),
        );
      }

    case 'options':
      $limit = drupal_map_assoc(array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
      $form['count'] = array(
        '#type' => 'select',
        '#title' => t('Maintainers'),
        '#default_value' => isset($options['count']) ? $options['count'] : 1,
        '#options' => $limit,
      );
      return $form;
  }
}
