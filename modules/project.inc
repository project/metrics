<?php

function metrics_project_metrics_functions() {
  return array(
    '_metrics_project_has_documentation',
  );
}

function _metrics_project_has_documentation($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('Project: Has documentation link'),
        'description' => t('Returns 1 if there is a valid link to documentation, else 0.'),
      );

    case 'compute':
      if ($node->type = 'project-project') {
        if (valid_url($node->documentation)) {
          return array(
            'value' => 1, 
            'description' => t('There is documentation for this project.'),
          );
        }
        else {
          return array(
            'value' => 0, 
            'description' => t('There is no documentation link for this project.'),
          );
        }
      }
  }
}
