<?php

function metrics_project_release_metrics_functions() {
  return array(
    '_metrics_project_release_has_official',
  );
}

function _metrics_project_release_has_official($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('Project release: Has official release'),
        'description' => t('Returns 1 if there is a published official release for the specificed version, else -1.'),
      );

    case 'compute':
      if (isset($options['tid'])) {
        $count = db_result(db_query('SELECT COUNT(*) FROM {node} n INNER JOIN {project_release_nodes} prn ON n.nid = prn.nid INNER JOIN {term_node} t ON prn.nid = t.nid WHERE n.status = 1 AND prn.rebuild = 0 AND prn.version_extra IS NULL AND t.tid = %d AND prn.pid = %d', $options['tid'], $node->nid));
        if ($count) {
          return array(
            'value' => 1, 
            'description' => t('There is an official release for this version.'),
          );
        }
        else {
          return array(
            'value' => -1, 
            'description' => t('There is no published, official release for this version.'),
          );
        }
      }

    case 'options':
      $terms = array();
      foreach (project_release_get_api_taxonomy() as $term) {
        $terms[$term->tid] = $term->name;
      }
      $form['tid'] = array(
        '#type' => 'select',
        '#title' => t('Version'),
        '#default_value' => $options['tid'],
        '#options' => $terms,
      );
      return $form;
  }
}
