<?php

/**
 * wonky name because we preface all the .inc files get called by:
 * metrics_ name _metrics_functions()
 */
function metrics_metrics_metrics_functions() {
  return array(
    '_metrics_test_one',
    '_metrics_test_zero',
    '_metrics_test_id',
  );
}

function _metrics_test_one($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('Metrics Test: Always one'),
        'description' => t('This always returns one.'),
      );

    case 'compute':
      return array(
        'value' => 0, 
        'description' => t('It got a one... we always return 1.'),
      );
  }
}

function _metrics_test_zero($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('Metrics Test: Always zero'),
        'description' => t('This always returns zero.'),
      );

    case 'compute':
      return array(
        'value' => 0, 
        'description' => t('It got zero... we always return 0.'),
      );
  }
}

function _metrics_test_id($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('Metrics Test: Node ID'),
        'description' => t("This returns the node's ID."),
      );

    case 'compute':
      return array(
        'value' => $node->nid,
        'description' => t("This node has an id of @nid.", array('@nid' => $node->nid)),
      );
  }
}
