<?php

function metrics_project_usage_metrics_functions() {
  return array(
    '_metrics_project_usage_weekly_log',
  );
}

/**
 * Metric to determine a project or release node node's usage.
 */
function _metrics_project_usage_weekly_log($op, $options = NULL, $node = NULL) {
  switch ($op) {
    case 'info':
      return array(
        'name' => t('Project usage: weekly usage'),
        'description' => t("Returns a score based on the number of sites using a project or release."),
      );

    case 'compute':
      $weeks = isset($options['weeks']) ? (int) $options['weeks'] : 1;
      $timestamp = mktime(0, 0, 0, $d['mon'], $d['mday'] - $d['wday'] - (7 * $weeks), $d['year']);

      switch ($node->type) {
        case 'project_project':
          $count = db_result(db_query("SELECT SUM(count) FROM {project_usage_week_project} WHERE nid = %d AND timestamp >= %d", $node->nid, $timestamp));
          break;

        case 'project_release':
          $count = db_result(db_query("SELECT SUM(count) FROM {project_usage_week_release} WHERE nid = %d AND timestamp >= %d", $node->nid, $timestamp));
          break;

        default:
          $count = 0;
      }
      if ($count) {
        return array(
          'value' => log($result),
          'description' => t('This project is used on !sites in the last !weeks.', array(
            '!sites' => format_plural($count, '1 site', '@count sites'),
            '!weeks' => format_plural($weeks, 'week', '@count weeks'),
          )),
        );
      }
      return NULL;

    case 'options':
      $weeks = drupal_map_assoc(array(1, 2, 3, 4, 6, 8, 10, 20, 26, 52));
      $form['weeks'] = array(
        '#type' => 'select',
        '#title' => t('Weeks'),
        '#default_value' => isset($options['weeks']) ? $options['weeks'] : 1,
        '#options' => $weeks,
      );
      return $form;
  }
}
